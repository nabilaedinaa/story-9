from django.test import TestCase, LiveServerTestCase
from django.test.client import Client
from django.urls import resolve
from .views import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time


class UnitTest(TestCase):
    def test_index_page_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_index_template_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
    
    def test_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

class FunctionalTest(LiveServerTestCase):
    def setUp(self) :
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.driver = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest,self).setUp()

    def tearDown(self):
        self.driver.quit()
        super(FunctionalTest,self).tearDown()

    def test_search_have_results(self):
        self.driver.get(self.live_server_url)
        self.driver.find_element_by_id("input").send_keys("a")
        time.sleep(8)
        result = self.driver.find_element_by_id("books")
        self.assertIn("Autobiography", result.text)

    def test_like_button_(self):
        self.driver.get(self.live_server_url)
        self.driver.find_element_by_id("input").send_keys("a")
        time.sleep(2)
        self.driver.find_element_by_class_name("fa-thumbs-up").click()
        count = self.driver.find_element_by_id("like0")
        self.assertEqual("1", count.text)
    
    def test_unlike_button_(self):
        self.driver.get(self.live_server_url)
        self.driver.find_element_by_id("input").send_keys("a")
        time.sleep(2)
        self.driver.find_element_by_class_name("fa-thumbs-down").click()
        count = self.driver.find_element_by_id("like0")
        self.assertEqual("0", count.text)
    
    def test_sort_and_modal_update(self):
        self.driver.get(self.live_server_url)
        self.driver.find_element_by_id("input").send_keys("a")
        time.sleep(2)
        self.driver.find_element_by_class_name("fa-thumbs-up").click()
        self.driver.find_element_by_class_name("fa-thumbs-up").click()
        self.driver.find_element_by_class_name("books-liked").click()
        time.sleep(1)
        first = self.driver.find_element_by_class_name("modal-body")
        self.assertIn("2", first.text)




    




