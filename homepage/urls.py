from django.urls import path
from .views import *

app_name = 'homepage'

urlpatterns = [
    path('', index, name='index'),
    path('api/like/', liked, name='liked'),
    path('api/unlike/', unliked, name='unliked'),
    path('api/top/', topbooks, name='topbooks'),
]
