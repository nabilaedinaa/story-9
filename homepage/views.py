from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from .models import *
import json

# Create your views here.
def index(request):
    return render(request, 'index.html')

@csrf_exempt
def liked(request):
    data = json.loads(request.body)
    try:
        book = Books.objects.get(book_id= data['id'])
        book.like += 1
        book.save()
    except:
        if 'authors' not in data['volumeInfo']:
            data['volumeInfo']['authors'] = "undefined"
        book = Books.objects.create(
            book_id = data['id'],
            image_link = data['volumeInfo']['imageLinks']['thumbnail'],
            title = data['volumeInfo']['title'],
            authors = data['volumeInfo']['authors'],
            like = 1
        )
    return JsonResponse({
        'likeCount' : book.like
    })


@csrf_exempt
def unliked(request):
    data = json.loads(request.body)
    try:
        book = Books.objects.get(book_id= data['id'])
        if book.like > 0:
            book.like -= 1
        book.save()
    except:
        if 'authors' not in data['volumeInfo']:
            data['volumeInfo']['authors'] = "undefined"
        book = Books.objects.create(
            book_id = data['id'],
            image_link = data['volumeInfo']['imageLinks']['thumbnail'],
            title = data['volumeInfo']['title'],
            authors = data['volumeInfo']['authors']
        )
    return JsonResponse({
        'likeCount' : book.like
    })

def topbooks(request):
    books = Books.objects.order_by('-like')

    book_list = []
    for book in books:
        book_list.append({
            'image_link': book.image_link,
            'title': book.title,
            'authors': book.authors,
            'like': book.like,
        })

    return JsonResponse({
        'books' : book_list
    })