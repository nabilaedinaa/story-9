var booksData = [];

$(document).ready(function() {
    $('#input').on("keyup",function (event) {
        event.preventDefault();
        const q = $('#input').val();
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var data = JSON.parse(this.responseText);
                console.log(data);
                var result = data.items;
                booksData = result;
                $("#books").html("");
                for(var i = 0; i < result.length; i++) {
                    $("#books").append(
                        "<tr>" +
                            "<td>" + "<img src= '"+ result[i].volumeInfo.imageLinks.thumbnail + "'>" + "</td>" +
                            "<td class='table-heading align-middle text-center'>" + result[i].volumeInfo.title + "</td>" +
                            "<td class='table-heading align-middle text-center'>" + result[i].volumeInfo.authors + "</td>" +
                            "<td class='table-heading align-middle text-center'>" + 
                            "<button type='button' onclick='like("+ i +")'><i class='fas fa-thumbs-up'></i></button>" +
                            "<button type='button' onclick='unlike("+ i +")'><i class='fas fa-thumbs-down'></i></button>" +
                            "<p id='like" + i + "' data value='0'></p></td>" +
                        "</tr>"
                    )
                }
            }
        };
        xhttp.open("GET", "https://www.googleapis.com/books/v1/volumes?q=" + q, true);
        xhttp.send();
    });
});

function like(i) {
    var sendData = JSON.stringify(booksData[i]);
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var like = JSON.parse(this.responseText).likeCount;
            $('#like'+i).html(like);
        }
    }
    xhttp.open("POST", "/api/like/");
    xhttp.send(sendData);
}

function unlike(i) {
    var sendData = JSON.stringify(booksData[i]);
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var like = JSON.parse(this.responseText).likeCount;
            $('#like'+i).html(like);
        }
    }
    xhttp.open("POST", "/api/unlike/");
    xhttp.send(sendData);
}

function removeBrackets(x){
    x = x.replace(/[\[\]']+/g, '');
}

function topfive() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var books = JSON.parse(this.responseText).books;
            $('.modal-body').html('');
            for (var i = 0; i < 5; i++) {
                var authorWithoutBrackets = books[i]['authors'].replace(/[\[\]']+/g, '');
                $(".modal-body").append(
                    "<p>" + "<img src= '"+ books[i]['image_link'] + "'>" + "</p>" +
                    "<p>Title : " + books[i]['title'] + "</p>" +
                    "<p>Author(s) : " + authorWithoutBrackets + "</p>" +
                    "<p><i class='fas fa-thumbs-up'></i> " + books[i]['like'] + "</p>"
                )
            }
        }
    }
    xhttp.open("GET", "/api/top/", true);
    xhttp.send();
}
